export interface CountryCovid {
  country: string;
  cases: number;
  todayCases: number;
  deaths: string;
  todayDeaths: string;
  recovered: number;
  active: number;
  critical: string;
  tests: string;
}
