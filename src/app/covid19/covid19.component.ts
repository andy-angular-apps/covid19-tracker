import { Component, OnInit, ViewChild } from '@angular/core';
import { Covid19Service } from '../covid19.service';
import { CountryCovid } from '../model/country-covid.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-covid19',
  templateUrl: './covid19.component.html',
  styleUrls: ['./covid19.component.css']
})
export class Covid19Component implements OnInit {

  ELEMENT_DATA: CountryCovid[];
  displayedColumns: string[] = ['country', 'cases', 'todayCases', 'deaths', 'todayDeaths', 'recovered', 'active', 'critical', 'tests'];
  dataSource = new MatTableDataSource<CountryCovid>(this.ELEMENT_DATA);

  constructor(private covidService: Covid19Service) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getAllData();
  }

  public getAllData() {
    let response = this.covidService.covid19Reports();
    response.subscribe(report => this.dataSource.data = report as CountryCovid[])
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
