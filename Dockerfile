FROM node:lts AS angular-covid-tracker
WORKDIR /app
COPY package.json /app
RUN npm install --silent
COPY . .
RUN npm run build

FROM nginx:alpine
VOLUME [ "/var/cache/nginx" ]
COPY --from=angular-covid-tracker app/dist/covid19-tracker /usr/share/nginx/html
COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf
